# setHostnamePls

Takes the FQDN to be set on the machine as it's first argument and modifies /etc/hostname, /etc/hosts and various other files both directly and indirectly using commands such as hostnamectl. The purpose of this script is to have a general command that will set your hostname across multiple distros in one go. 
