# Change Log

## Version 1.0-1 (Version 2)

	gpg: Signature made Fri 17 Jan 2020 22:31:08 AEDT
	gpg:                using RSA key 2C3EBF239A279270E5399EF65DE5F96B92FA7361
	gpg: Good signature from "Conor Andrew Buckley" [ultimate]
	gpg:                 aka "[jpeg image of size 5400]" [ultimate]

	sethostnamepls (1.0-1) unstable; urgency=medium

	  * Initial release.

	 -- Conor Andrew Buckley <noreply@conorab.com>  Wed, 15 Jan 2020 20:15:00 +1100

Last updated according to www.conorab.com: 2020-01-15

## Version 1 

	gpg: Signature made Sun 15 Dec 2019 22:44:55 AEDT
	gpg:                using RSA key E4F7B1B5A97074A0
	gpg: Good signature from "Conor Andrew Buckley - Archive ES 1 (Used to sign and encrypt files which will not be changed.)" [ultimate]
